import Vue from "vue";
import VueRouter from "vue-router";
import Tabbar from '../views/Tabbar'
import Index from '../views/index/Index'
import {
  Dialog
} from 'vant'
import store from '../store'

Vue.use(VueRouter);

const routes = [{
    path: '/login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/',
    component: Tabbar,
    children: [{
        path: '',
        component: Index
      },
      {
        path: 'questions',
        component: () => import('../views/questions/Index.vue')
      },
      {
        path: 'video',
        component: () => import('../views/video/Index.vue')
      },
      {
        path: 'me',
        component: () => import('../views/me/Index.vue'),
        meta: {
          mustLogin: true // 这个页面必须登录才能访问
        }
      }
    ]
  },
  {
    path: '/content/:id',
    component: () => import('../views/index/Content.vue')
  },
  {
    path: '/fankui',
    component: () => import('../views/me/Fankui.vue'),
    meta: {
      mustLogin: true // 这个页面必须登录才能访问
    }
  },
  {
    path: '/xiaozhi',
    component: () => import('../views/me/Xiaozhi.vue'),
    meta: {
      mustLogin: true // 这个页面必须登录才能访问
    }
  },
  {
    path: '/profile',
    component: () => import('../views/my/Profile.vue'),
    meta: {
      mustLogin: true // 这个页面必须登录才能访问
    }
  },
  {
    path: '/edit_profile',
    component: () => import('../views/my/EditProfile.vue'),
    meta: {
      mustLogin: true // 这个页面必须登录才能访问
    }
  }
];

router.beforeEach(function (to, from, next) {
  // 判断下一个页面上是否标记了必须要登录
  if (to.meta.mustLogin) {
    // 判断登录
    if (store.state.user) {
      // 通过
      next()
    } else {
      // 弹出对话框
      Dialog.confirm({
        message: '必须登录才能访问，是否现在去登录？'
      }).then(() => {
        // 跳转到登录页面
        next('/login')
      }).catch(() => {
        next(false)
      })
    }
  } else {
    // 通过
    next()
  }
})

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router;