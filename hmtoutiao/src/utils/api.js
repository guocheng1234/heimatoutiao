import request from './request.js'

// 发送验证码
export const sendCode = (mobile) => {
  return request.get('/v1_0/sms/codes/' + mobile)
}

// 登录
export const login = (mobile, code) => {
  return request.post('/v1_0/authorizations', {mobile, code})
}

// 获取全部新闻频道
export const getAllChannels = () => {
  return request.get('/v1_0/channels')
}

// 获取用户的频道
export const getUserChannels = () => {
  return request.get('/v1_0/user/channels')
}

// 删除用户的频道
export const deleteUserChannel = channelId => {
  return request.delete('/v1_0/user/channels/' + channelId)
}

// 添加用户频道
export const addUserChannel = data => {
  return request.patch('/v1_0/user/channels', data)
}

// 获取某一个频道下面的新闻
export const getChannelArticles = (channelId, page, perPage) => {
  let timestamp = Date.now()
  return request.get(`/v1_1/articles?channel_id=${channelId}&timestamp=${timestamp}&with_top=1&page=${page}&per_page=${perPage}`)
}

// 根据 ID 获取文章详情
export const getArticleById = artId => {
  return request.get('/v1_0/articles/' + artId)
}

// 关注作者
export const followById = userId => {
  // 在 body 中传参数
  return request.post('/v1_0/user/followings', {target: userId})
}

// 取消关注作者
export const deleteFollowById = userId => {
  // 在 url 上传参数
  return request.delete('/v1_0/user/followings/' + userId)
}

// 获取文章的评论
// artId：文章ID
// offset：翻页用的ID，第一页时不传
export const getCommentsByArtId = (artId, offset) => {
  let params = {
    type: 'a',
    source: artId
  }
  // 如果有 offset 就传参数
  if (offset) {
    params.offset = offset
  }
  return request.get('/v1_0/comments', { params })
}

// 发表评论
export const addComment = (target, content) => {
  return request.post('/v1_0/comments', {
    target,
    content
  })
}

// 对评论点赞
export const zanComment = target => {
  return request.post('/v1_0/comment/likings', {
    target
  })
}

// 对评论取消点赞
export const cancelZanComment = target => {
  return request.delete('/v1_0/comment/likings/' + target)
}

// 修改头像
export const uploadAvatar = photo => {
  // 上传文件时一般需要创建一个 FormData 再提交
  let form = new FormData()
  // 参数一、接口文档上规定的参数名
  // 参数二、本地图片的令牌
  form.append('photo', photo)
  // 调用接口上传
  return request.patch('/v1_0/user/photo', form)
}

// 获取我的信息
export const getMyInfo = () => {
  return request.get('/v1_0/user')
}

// 更新个人资料
export const updateProfile = data => {
  return request.patch('/v1_0/user/profile', data)
}