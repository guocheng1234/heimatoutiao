import Vue from 'vue'

import {
  Button,
  Form,
  Field,
  Icon,
  CountDown,
  Toast,
  Tabbar,
  TabbarItem,
  NavBar,
  Cell,
  CellGroup,
  Tab,
  Tabs,
  List,
  Popup,
  Grid,
  GridItem,
  Tag,
  Image,
  Lazyload,
  PullRefresh,
  ActionSheet,
  Uploader,
  RadioGroup,
  Radio,
  Picker
} from 'vant'

Vue.use(Button)
  .use(Form)
  .use(Field)
  .use(Icon)
  .use(CountDown)
  .use(Toast)
  .use(Cell)
  .use(CellGroup)
  .use(Tabbar)
  .use(TabbarItem)
  .use(NavBar)
  .use(Tab)
  .use(Tabs)
  .use(List)
  .use(Popup)
  .use(Grid)
  .use(GridItem)
  .use(Tag)
  .use(Image)
  .use(PullRefresh)
  .use(ActionSheet)
  .use(Uploader)
  .use(RadioGroup)
  .use(Radio)
  .use(Picker)
  .use(Lazyload, {
    lazyComponent: true
  })