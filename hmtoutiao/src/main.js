import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import './utils/register'
import './utils/dayjs'

// 配置 REM 适配
import 'amfe-flexible'

// 引入公共样式
import './styles/index.less'

import request from './utils/request'
Vue.prototype.$axios = request

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");