import Vue from 'vue'
import Vuex from 'vuex'
import {
  getMyInfo
} from '../utils/api'

Vue.use(Vuex)

export default new Vuex.Store({
  // 定义在保存的数据
  state: {
    // 从本地存储中读取并转成一个对象
    user: JSON.parse(localStorage.getItem('user')),
    // 用户的详细信息
    userInfo: {}
  },
  // 定义修改 state 中数据的函数
  mutations: {
    setUser(state, data) {
      state.user = data
      // 写入浏览器（本地存储）
      localStorage.setItem('user', JSON.stringify(data))
    },
    // 更新 token
    setToken(state, token) {
      state.user.token = token
      // 更新本地存储
      localStorage.setItem('user', JSON.stringify(state.user))
    },
    // 清空 user
    logout(state) {
      state.user = null
      localStorage.removeItem('user')
    },
    // 修改 userInfo 的
    setUserInfo(state, data) {
      state.userInfo = data
    }
  },
  actions: {
    // 获取用户信息
    getUserInfo(context) {
      // 获取接口获取用户信息
      getMyInfo().then(res => {
        // 把得到的用户信息保存到 state.userInfo 中
        // 调用 mutations 中的 setUserInfo 来修改 state.userInfo （state 中的数据必须要通过 mustions 来修改）
        context.commit('setUserInfo', res.data.data)
      })
    }
  },
  modules: {}
})